Red Hat Decision Manager Install Demo (WildFly FORK) 
====================================================
Project to automate the installation of this product with simple standalone configuration.

FORK of [rhdm-install-demo](https://gitlab.com/bpmworkshop/rhdm-install-demo/) that installs Red Hat Decision Manager on WildFly instead of Jboss EAP-7.2 

Install on your machine
----------------------------------
1. [Download and unzip.](https://gitlab.com/antonpinchuk/rhdm-install-demo/-/archive/master/rhdm-install-demo-master.zip)

2. Add products to installs directory, see installs/README for details and links.

3. Run 'init.sh' or 'init.bat' file. 'init.bat' must be run with Administrative privileges

4. Login to http://localhost:8080/decision-central (u:admin / p:admin)

5. Kie Servier access configured (u: kieserver / p:kieserver)

6. Enjoy installed and configured Red Hat Decision Manager.

